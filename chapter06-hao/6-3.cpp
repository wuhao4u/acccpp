#include <iostream>
#include <vector>
#include <iterator>

using std::vector;
using std::endl;
using std::cout;
using std::cin;

void print_vector(const vector<int> vec){
    for(vector<int>::const_iterator it = vec.begin(); it != vec.end(); ++it){
        cout << *it << endl;
    }
}

int main() {
    vector<int> u(10,100);
    // vector<int> u(10);
    vector<int> v1, v2;
    // copy(u.begin(), u.end(), v.begin()); // this will crash the program since v.size() is 0.
    cout << "vector u: " << endl;
    print_vector(u);

    // 6-4 1st way with back_inserter
    copy(u.begin(), u.end(), back_inserter(v1));
    cout << "vector v back_inserter: " << endl;
    print_vector(v1);

    // 6-4 2nd way with inserter, this will insert the new element in front of 'it'
    cout << "vector v inserter: " << endl;
    vector<int>::iterator it = v2.begin();
    copy(u.begin(), u.end(), inserter(v2, it));
    print_vector(v2);
}